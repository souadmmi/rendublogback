package co.simplon.blog.entity;

import java.time.LocalDateTime;

public class Comment {

    private int id;
    private String content;
    private LocalDateTime date;
    private String author;

    
    public Comment() {
    }
    public Comment(String content, LocalDateTime date, String author) {
        this.content = content;
        this.date = date;
        this.author = author;
    }
    public Comment(int id, String content, LocalDateTime date, String author) {
        this.id = id;
        this.content = content;
        this.date = date;
        this.author = author;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    


}
