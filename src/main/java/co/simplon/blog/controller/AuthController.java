package co.simplon.blog.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.blog.entity.User;
import co.simplon.blog.repository.UserRepository;
import jakarta.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    private PasswordEncoder hasher;
    
    @Autowired
    private UserRepository userRepo;

    @PostMapping("/api/user")
    public User register(@Valid @RequestBody User user) {

        if(userRepo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }

        String hash = hasher.encode(user.getPassword());
        user.setPassword(hash);
        user.setRole("ROLE_USER");
        userRepo.persist(user);
        // SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
        return user;
    }


    @GetMapping("/api/account")
    public User myAccount(@AuthenticationPrincipal User user) {
        return user;
    }

    @GetMapping("/api/user")
    public List<User> allUsers(){
        return userRepo.findAllUser();
    }
    @GetMapping("api/user/{id}")
    public User oneUser(@PathVariable int id){
        Optional <User> user = userRepo.findById(id);
        if (user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return user.get();
    }


    @DeleteMapping("api/user/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        oneUser(id); 
        userRepo.delete(id);
    }
    
    
}

