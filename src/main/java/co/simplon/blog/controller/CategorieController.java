package co.simplon.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.blog.entity.Categorie;
import co.simplon.blog.repository.CategorieRepository;
import jakarta.validation.Valid;

@CrossOrigin("*")
@RequestMapping("/api/categorie")
@RestController 
public class CategorieController {

    @Autowired 
    private CategorieRepository repo;

    @GetMapping
    public List<Categorie> all(){
        return repo.findAll();
        
    }

    @GetMapping("/{id}")
    public Categorie onCategorie(@PathVariable int id){
        Categorie categorie = repo.findbyId(id);
        if (categorie == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return categorie;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categorie add(@RequestBody Categorie categorie) {
        repo.persist(categorie);
        return categorie;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        onCategorie(id); 
        repo.delete(id);
    }

    @PutMapping("/{id}")
    public Categorie replace(@PathVariable int id, @Valid @RequestBody Categorie categorie) {
        onCategorie(id);
        categorie.setId(id);
        repo.update(categorie);
        return categorie;
    }


}
