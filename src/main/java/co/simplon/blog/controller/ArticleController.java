package co.simplon.blog.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.blog.entity.Article;
import co.simplon.blog.entity.User;
import co.simplon.blog.repository.ArticleRepository;
import jakarta.validation.Valid;

@CrossOrigin("*")
@RequestMapping("/api/article")
@RestController 

public class ArticleController {

    @Autowired 
    private ArticleRepository repo;

    @GetMapping
    public List<Article> all(){
        return repo.findAll();
        
    }



    @GetMapping("/categorie/{id}")
    public List<Article> allOfCategorie(@PathVariable int id ){
        return repo.findAllOfCategorie(id);
        
    }

    @GetMapping("/threelatest")
    public List<Article>ThreeLatest(){
        return repo.findThreeLatest();
    }

    @GetMapping("/{id}")
    public Article onArticle(@PathVariable int id){
        Article article = repo.findbyId(id);
        if (article == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        article.setVue(article.getVue()+1);
        repo.update(article);
        return article;
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article add(@RequestBody Article article, @AuthenticationPrincipal User user ) {
        article.setUser( user);
        article.setDate(LocalDateTime.now());
        repo.persist(article);
        return article;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        Article article = onArticle(id);
        //Si on est pas admin et que l'address ne nous appartient pas, on renvoie une erreur
        if(!user.getRole().equals("ROLE_ADMIN") && !article.getUser().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        repo.delete(id);
    }

    @PutMapping("/{id}")
    public Article replace(@PathVariable int id, @Valid @RequestBody Article article, @AuthenticationPrincipal User user) {
        onArticle(id);
        if(!user.getRole().equals("ROLE_ADMIN") && !article.getUser().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        article.setId(id);
        repo.update(article);
        return article;

        /*
         * Article article = onArticle(id)
         * 
         */
    }

    @GetMapping("/user")
    public List<Article> allArticlesOfUser( @AuthenticationPrincipal User user){
        Integer id = user.getId();
        return repo.findAllArticleOfOneUser(id);
        
        }

    

}
