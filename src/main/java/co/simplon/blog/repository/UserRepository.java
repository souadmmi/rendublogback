package co.simplon.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ArrayList;
import java.util.List;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.blog.entity.User;

@Repository
public class UserRepository {

    @Autowired
    private DataSource dataSource;


    public List<User> findAllUser(){
        List<User> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM user");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                User article = new User(
                    result.getInt("id"), 
                        result.getString("email"),
                        result.getString("name"),
                        result.getString("password"), 
                        result.getString("role"));
                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }
    

    public Optional<User> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return Optional.of(
                    new User(
                        result.getInt("id"), 
                        result.getString("email"),
                        result.getString("name"),
                        result.getString("password"), 
                        result.getString("role"))
                    );
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    public Optional<User> findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return Optional.of(
                    new User(
                        result.getInt("id"), 
                        result.getString("email"),
                        result.getString("name"),
                        result.getString("password"), 
                        result.getString("role"))
                    );
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    public boolean persist(User user) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO user (email, name, password,role) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getName());
            stmt.setString(3, user.getPassword());
            stmt.setString(4, user.getRole());
            stmt.executeUpdate();
            
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if(generatedKeys.next()) {
                user.setId(generatedKeys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return false;
    }

    public boolean update(User user){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET name=?, password=? WHERE id=?" ); // role ? protege ?
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getPassword());
            stmt.setInt(3, user.getId());
            
            if(stmt.executeUpdate()==1){
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return false;
    }

    public boolean delete (int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM user WHERE id=?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate()==1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return false;
    }



}
