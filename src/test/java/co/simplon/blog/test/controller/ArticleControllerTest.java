package co.simplon.blog.test.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc 
@Sql("/database.sql")
public class ArticleControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {

        mvc.perform(get("/api/article"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['content']").exists())
        .andExpect(jsonPath("$[0]['title']").value("Les 3 sensations alimentaires"));
    }

    @Test
    void testGetThreeLatest() throws Exception {
        mvc.perform(get("/api/article/threelatest"))
        .andExpect(status().isOk());
    }

    @Test
    void testGetAllOfCategorie() throws Exception {
        mvc.perform(get("/api/article/categorie/1"))
        .andExpect(status().isOk());
    }




    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/article/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['title']").isString())
        .andExpect(jsonPath("$['content']").isString())
        .andExpect(jsonPath("$['date']").exists());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/article/100000000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostArticle() throws Exception {
        mvc.perform(
            post("/api/article")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "title": "miaoudiniwniw",
                    "content": "un essaie",
                    "picture": "htpps//bonsoir.com",
                    "date": "2024-05-14T18:15:00",
                    "vue": 0,
                    "author": "Souad Mhamdi",
                    "categorie":  {"id":1}
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testDeleteArticle() throws Exception {
        mvc.perform(delete("/api/article/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutArticle() throws Exception {
        mvc.perform(put("/api/article/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                    "id":1,
                    "title": "miaoudiniwniw",
                    "content": "un essai",
                    "picture": "htpps//bonsoir.com",
                    "date": "2024-05-14T18:15:00",
                    "vue": 0,
                    "author": "Souad Mhamdi",
                    "categorie": {"id":1}
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['title']").value("miaoudiniwniw"));
    }


}
