package co.simplon.blog.test.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc 
@Sql("/database.sql")

public class CategorieControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/categorie"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['id']").exists())
        .andExpect(jsonPath("$[0]['name']").value("Forme & Bien-être"));
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/categorie/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['name']").isString());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/categorie/100000000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostCategorie() throws Exception {
        mvc.perform(
            post("/api/categorie")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "name": "miaoudiniwniw"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testDeleteCategorie() throws Exception {
        mvc.perform(delete("/api/categorie/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutCategorie() throws Exception {
        mvc.perform(put("/api/categorie/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                    "name":"souad"
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['name']").value("souad"));
    }


}
