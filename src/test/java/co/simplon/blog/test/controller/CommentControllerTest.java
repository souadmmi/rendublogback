package co.simplon.blog.test.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc 
@Sql("/database.sql")

public class CommentControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {

        mvc.perform(get("/api/comment"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['content']").exists())
        .andExpect(jsonPath("$[0]['author']").value("Jean Mi"));
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/comment/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['content']").isString())
        .andExpect(jsonPath("$['author']").isString())
        .andExpect(jsonPath("$['date']").exists());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/comment/100000000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostComment() throws Exception {
        mvc.perform(
            post("/api/comment")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "content": "Super article",
                    "date": "2024-03-14T18:15:00",
                    "author": "Jean Mi"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testDeleteComment() throws Exception {
        mvc.perform(delete("/api/comment/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutComment() throws Exception {
        mvc.perform(put("/api/comment/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "id": 1,
                "content": "Trop nul l'article",
                "date": "2024-03-14T18:15:00",
                "author": "Miaou"
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['author']").value("Miaou"));
    }
}
